// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.2 (swiftlang-1103.0.32.6 clang-1103.0.32.51)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GBGSmartCapture
import Foundation
import GBGCore
import MJCS
import Moya
import ObjectMapper
import Swift
import UIKit
open class GBGVerifyDocumentService : GBGCore.GBGBaseService<GBGSmartCapture.GBGVerifyDocumentAPI> {
  override public init()
  public func getDocumentDataDocumentTypes(success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGDocumentType>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func getDocumentImageDocumentTypes(success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGDocumentType>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func verifyDocumentData(documentRequestHeader: GBGSmartCapture.GBGVerifyDocumentDataRequestHeader, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGVerificationResultResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func verifyDocumentDataId(_ id: Swift.String, documentRequestHeader: GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGVerificationResultResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func verifyDocumentImage(_ document: GBGCore.GBGDocument, documentRequestHeader: GBGSmartCapture.GBGVerifyDocumentImageRequestHeader, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGVerificationResultResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func verifyDocumentImageId(_ id: Swift.String, document: GBGCore.GBGDocument, documentRequestHeader: GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGVerificationResultResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  public func getImage(id: Swift.String, personId: Swift.String, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGSmartCapture.GBGVerifyDocumentImageResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  @objc deinit
  override public init(plugins: [Moya.PluginType] = super, timeout: Foundation.TimeInterval = super, cachePolicy: Foundation.NSURLRequest.CachePolicy = super)
}
@objc @_inheritsConvenienceInitializers public class GBGVerifyDocumentScannerViewController : MJCS.IDSDocumentScannerController {
  @objc deinit
  @objc override dynamic public init(scannerConfig config: MJCS.IDSDocumentScannerConfig)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
public struct GBGDocumentVerificationResponse : ObjectMapper.Mappable, Swift.Equatable {
  public var timestamp: Swift.String?
  public var customerReference: Swift.String?
  public var action: Swift.String?
  public var verificationId: Swift.String?
  public var combined: GBGCore.GBGCombined?
  public var verification: GBGCore.GBGVerification?
  public var triangulation: GBGCore.GBGTriangulation?
  public var links: [GBGCore.GBGLink]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGSmartCapture.GBGDocumentVerificationResponse, b: GBGSmartCapture.GBGDocumentVerificationResponse) -> Swift.Bool
}
@_hasMissingDesignatedInitializers public class GBGVerifyDocumentDataPersonRequestHeaderManager {
  @objc deinit
}
public class GBGVerifyDocumentImagePersonRequestHeader : GBGCore.Buildable {
  public var customerReference: Swift.String
  public var journeyId: Swift.String
  public var journeyVersion: Swift.String
  public var documentId: Swift.String
  public var triangulationVerificationId: Swift.String
  public var tolerance: Swift.String
  required public init()
  @discardableResult
  public func withJourneyId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @discardableResult
  public func withJourneyVersion(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @discardableResult
  public func withCustomerReference(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @discardableResult
  public func withTolerance(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @discardableResult
  public func withDocumentId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @discardableResult
  public func withTriangulationVerificationId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeader
  @objc deinit
}
public struct GBGVerifyDocumentImageResponse {
  public var image: UIKit.UIImage?
  public init(image: UIKit.UIImage)
  public init(data: Foundation.Data)
}
@_hasMissingDesignatedInitializers public class GBGVerifyDocumentDataRequestHeaderManager {
  @objc deinit
}
public enum GBGVerifyDocumentAPI {
  case getDocumentDataTypes(network: GBGCore.GBGNetworkModel)
  case getDocumentImageTypes(network: GBGCore.GBGNetworkModel)
  case verifyDocumentData(network: GBGCore.GBGNetworkModel, verifyDocumentDataRequestHeaderManager: GBGSmartCapture.GBGVerifyDocumentDataRequestHeaderManager)
  case verifyDocumentDataWithId(network: GBGCore.GBGNetworkModel, verifyDocumentDataPersonRequestHeaderManager: GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeaderManager)
  case verifyDocumentImage(network: GBGCore.GBGNetworkModel, document: GBGCore.GBGDocument, verifyDocumentImageRequestHeaderManager: GBGSmartCapture.GBGVerifyDocumentImageRequestHeaderManager)
  case verifyDocumentImageWithId(network: GBGCore.GBGNetworkModel, document: GBGCore.GBGDocument, verifyDocumentImagePersonPersonRequestHeaderManager: GBGSmartCapture.GBGVerifyDocumentImagePersonRequestHeaderManager)
  case getVerifyDocumentImageWithId(network: GBGCore.GBGNetworkModel)
}
extension GBGVerifyDocumentAPI : Moya.TargetType, Moya.AccessTokenAuthorizable, GBGCore.TaskManagable, GBGCore.AuthTokenManagable {
  public var baseURL: Foundation.URL {
    get
  }
  public var headers: [Swift.String : Swift.String]? {
    get
  }
  public var authorizationType: Moya.AuthorizationType? {
    get
  }
  public var path: Swift.String {
    get
  }
  public var method: Moya.Method {
    get
  }
  public var sampleData: Foundation.Data {
    get
  }
  public var task: Moya.Task {
    get
  }
}
public class GBGSmartCapture : GBGCore.ModuleItem {
  required public init()
  @objc deinit
}
public enum GBGTolerance : Swift.String, Swift.Equatable {
  case `default`
  case medium
  case strict
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@_hasMissingDesignatedInitializers public class GBGVerifyDocumentImagePersonRequestHeaderManager {
  @objc deinit
}
public class GBGVerifyDocumentImageRequestHeader : GBGCore.Buildable {
  public var tolerance: Swift.String
  public var customerReference: Swift.String
  public var journeyId: Swift.String
  public var journeyVersion: Swift.String
  required public init()
  @discardableResult
  public func withTolerance(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImageRequestHeader
  @discardableResult
  public func withJourneyId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImageRequestHeader
  @discardableResult
  public func withJourneyVersion(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImageRequestHeader
  @discardableResult
  public func withCustomerReference(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentImageRequestHeader
  @objc deinit
}
@_hasMissingDesignatedInitializers public class GBGVerifyDocumentImageRequestHeaderManager {
  @objc deinit
}
public protocol GBGVerifyDocumentScannerDelegate : AnyObject {
  func documentScannerController(didFinishScanning model: GBGSmartCapture.GBGVerifyDocumentScannerModel?)
  func documentScannerControllerDidCancel()
  func documentScannerController(didFailWithError error: Swift.Error?)
}
public class GBGVerifyDocumentRequestHeader : GBGCore.Buildable {
  public var personId: Swift.String
  public var gbgCorrelationId: Swift.String
  public var customerReference: Swift.String
  public var journeyId: Swift.String
  public var journeyVersion: Swift.String
  public var documentId: Swift.String
  public var triangulationVerificationId: Swift.String
  public var tolerance: Swift.String
  required public init()
  @discardableResult
  public func withPersonId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withGbgCorrelationId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withJourneyId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withJourneyVersion(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withCustomerReference(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withTolerance(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withDocumentId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @discardableResult
  public func withTriangulationVerificationId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentRequestHeader
  @objc deinit
}
public class GBGVerifyDocumentDataRequestHeader : GBGCore.Buildable {
  public var cleanseAddress: Swift.Bool
  public var customerReference: Swift.String
  public var journeyId: Swift.String
  public var journeyVersion: Swift.String
  required public init()
  @discardableResult
  public func withCleanAddress(_ value: Swift.Bool) -> GBGSmartCapture.GBGVerifyDocumentDataRequestHeader
  @discardableResult
  public func withJourneyId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataRequestHeader
  @discardableResult
  public func withJourneyVersion(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataRequestHeader
  @discardableResult
  public func withCustomerReference(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataRequestHeader
  @objc deinit
}
public struct GBGVerifyDocumentScannerModel {
  public let extractedImage: UIKit.UIImage!
  public let document: MJCS.IDESDocument?
}
public class GBGVerifyProfileManager {
  public init()
  public func installProfile(path: Swift.String, tag: Swift.String) throws
  public func swapProfile(profile: MJCS.IDESProfile, completion: @escaping (Swift.Bool, Foundation.NSException?) -> Swift.Void)
  public func swapProfile(tag: Swift.String, completion: @escaping (Swift.Bool, Foundation.NSException?) -> Swift.Void)
  public func removeProfile(profile: MJCS.IDESProfile) -> Swift.Bool
  public func removeProfile(tag: Swift.String) -> Swift.Bool
  public func getProfile(tag: Swift.String) -> MJCS.IDESProfile?
  public func listProfiles() -> [MJCS.IDESProfile]
  public func getProfileTagList() -> [Swift.String]
  public func loadDefaultProfile(completion: @escaping (Swift.Bool, Foundation.NSException?) -> Swift.Void)
  public func setProcessingMode(profile: MJCS.IDESProfile, processingMode: MJCS.IDESProcessingMode)
  @objc deinit
}
public class GBGVerifyDocumentDataPersonRequestHeader : GBGCore.Buildable {
  public var cleanseAddress: Swift.Bool
  public var customerReference: Swift.String
  public var journeyId: Swift.String
  public var journeyVersion: Swift.String
  public var documentId: Swift.String
  public var triangulationVerificationId: Swift.String
  required public init()
  @discardableResult
  public func withCleanAddress(_ value: Swift.Bool) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @discardableResult
  public func withJourneyId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @discardableResult
  public func withJourneyVersion(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @discardableResult
  public func withCustomerReference(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @discardableResult
  public func withDocumentId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @discardableResult
  public func withTriangulationVerificationId(_ value: Swift.String) -> GBGSmartCapture.GBGVerifyDocumentDataPersonRequestHeader
  @objc deinit
}
@objc public class GBGVerifyDocumentScanner : ObjectiveC.NSObject {
  required public init(scannerType: MJCS.IDSDocumentScannerType = .document, enableRecaptureDetection: Swift.Bool = false, config: MJCS.IDSDocumentScannerConfig = IDSDocumentScannerConfig.builder(), delegate: GBGSmartCapture.GBGVerifyDocumentScannerDelegate)
  public func setCustomLocalization(with tableName: Swift.String, in bundle: Foundation.Bundle? = nil)
  public func present()
  @objc deinit
  @objc override dynamic public init()
}
extension GBGVerifyDocumentScanner : MJCS.IDSDocumentScannerControllerDelegate {
  @objc dynamic public func documentScannerController(_ scanner: MJCS.IDSDocumentScannerController, didFinishScanningWithInfo info: [Swift.AnyHashable : Any])
  @objc dynamic public func documentScannerControllerDidCancel(_ scanner: MJCS.IDSDocumentScannerController)
  @objc dynamic public func documentScannerController(_ scanner: MJCS.IDSDocumentScannerController, didFailWithError error: Swift.Error?)
}
extension GBGSmartCapture.GBGTolerance : Swift.Hashable {}
extension GBGSmartCapture.GBGTolerance : Swift.RawRepresentable {}
