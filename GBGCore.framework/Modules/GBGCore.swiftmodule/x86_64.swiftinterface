// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.2 (swiftlang-1103.0.32.6 clang-1103.0.32.51)
// swift-module-flags: -target x86_64-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GBGCore
import Foundation
import Locksmith
import Moya
import ObjectMapper
import Swift
import UIKit
extension UIImage {
  public func convert(from mimeType: GBGCore.MimeType, compression: CoreGraphics.CGFloat = 1) -> Foundation.Data?
}
public struct GBGImageResponse : ObjectMapper.Mappable, Swift.Equatable {
  public var type: Swift.String?
  public var imageUrl: Swift.String?
  public var imageId: Swift.String?
  public var base64Image: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGImageResponse, b: GBGCore.GBGImageResponse) -> Swift.Bool
}
public struct GBGCombinedAnalysis : ObjectMapper.Mappable, Swift.Equatable {
  public var rules: [GBGCore.GBGRule]?
  public var decision: GBGCore.GBGDecision?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGCombinedAnalysis, b: GBGCore.GBGCombinedAnalysis) -> Swift.Bool
}
public struct GBGResponse<T> {
  public let response: Moya.Response
  public let body: T?
  public init(response: Moya.Response, body: T?)
}
@_hasMissingDesignatedInitializers open class GBGVerifySDK {
  public static func initialize(_ modules: GBGCore.ModuleItem.Type...)
  @objc deinit
}
public struct GBGVerification : ObjectMapper.Mappable, Swift.Equatable {
  public var derivedData: GBGCore.GBGDerivedData?
  public var analysis: [GBGCore.GBGAnalysis]?
  public var result: GBGCore.GBGResultResponse?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGVerification, b: GBGCore.GBGVerification) -> Swift.Bool
}
public struct GBGGbrDrivingLicence : ObjectMapper.Mappable, Swift.Equatable {
  public var photoIDFront: GBGCore.PhotoIDFront?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGGbrDrivingLicence, b: GBGCore.GBGGbrDrivingLicence) -> Swift.Bool
}
public struct PhotoIDFront : ObjectMapper.Mappable, Swift.Equatable {
  public var firstName: Swift.String?
  public var lastName: [Swift.String]?
  public var dateOfBirth: Swift.String?
  public var issueDate: Swift.String?
  public var expiryDate: Swift.String?
  public var documentNumber: Swift.String?
  public var address: GBGCore.GBGAddress?
  public var licenseCategory: Swift.String?
  public var portraitPhoto: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.PhotoIDFront, b: GBGCore.PhotoIDFront) -> Swift.Bool
}
public struct GBGPerson : ObjectMapper.Mappable, Swift.Equatable {
  public var title: Swift.String?
  public var firstName: Swift.String?
  public var middlenames: [Swift.String]?
  public var lastnames: [Swift.String]?
  public var gender: Swift.String?
  public var birthdate: Swift.String?
  public var address: GBGCore.GBGAddress?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGPerson, b: GBGCore.GBGPerson) -> Swift.Bool
}
public struct GBGRule : ObjectMapper.Mappable, Swift.Equatable {
  public var id: Swift.String?
  public var function: Swift.String?
  public var country: Swift.String?
  public var state: Swift.String?
  public var score: Swift.String?
  public var flag: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGRule, b: GBGCore.GBGRule) -> Swift.Bool
}
@_hasMissingDesignatedInitializers public class GBGOAuthCredentialsManager {
  @objc deinit
}
public struct GBGUKDrivingLicence : ObjectMapper.Mappable, Swift.Equatable {
  public var firstName: Swift.String?
  public var lastName: [Swift.String]?
  public var dateOfBirth: Swift.String?
  public var issueDate: Swift.String?
  public var expiryDate: Swift.String?
  public var documentNumber: Swift.String?
  public var address: GBGCore.GBGCleansed?
  public var licenseCategory: Swift.String?
  public var portraitPhoto: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGUKDrivingLicence, b: GBGCore.GBGUKDrivingLicence) -> Swift.Bool
}
public struct GBGEnrichment : ObjectMapper.Mappable, Swift.Equatable {
  public var cleansed: GBGCore.GBGCleansed?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGEnrichment, b: GBGCore.GBGEnrichment) -> Swift.Bool
}
public struct GBGDocumentTypeJourney : ObjectMapper.Mappable {
  public var journeyID: Swift.String?
  public var description: Swift.String?
  public var supportedVersions: [Swift.Int]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
}
public protocol TaskManagable {
  func upload(image: GBGCore.GBGImage) -> Moya.Task
  func upload<T>(body: T) -> Moya.Task where T : ObjectMapper.Mappable
  func upload(parameters: [Swift.String : Any], encoding: Moya.ParameterEncoding) -> Moya.Task
}
extension TaskManagable {
  public func upload(document: GBGCore.GBGDocument) -> Moya.Task
  public func upload(image: GBGCore.GBGImage) -> Moya.Task
  public func upload<T>(body: T) -> Moya.Task where T : ObjectMapper.Mappable
  public func upload(parameters: [Swift.String : Any], encoding: Moya.ParameterEncoding) -> Moya.Task
}
open class GBGOAuthService : GBGCore.GBGBaseService<GBGCore.GBGOAuthAPI> {
  public init(networkModel: GBGCore.GBGNetworkModel)
  public func retrieveToken(_ oauthCredentials: GBGCore.GBGOAuthCredentials, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGOAuthResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  @objc deinit
  override public init()
  override public init(plugins: [Moya.PluginType] = super, timeout: Foundation.TimeInterval = super, cachePolicy: Foundation.NSURLRequest.CachePolicy = super)
}
public typealias GBGSuccessResponseHandler<T> = (T?) -> Swift.Void
public typealias GBGFailureResponseHandler = (Swift.Error) -> Swift.Void
public struct GBGField : ObjectMapper.Mappable, Swift.Equatable {
  public var id: Swift.String?
  public var name: Swift.String?
  public var type: Swift.String?
  public var description: Swift.String?
  public var value: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGField, b: GBGCore.GBGField) -> Swift.Bool
}
public struct GBGImage {
  public let data: Foundation.Data?
  public let fileName: Swift.String!
  public let fileExtension: Swift.String!
  public let mimeType: GBGCore.MimeType!
  public init(image: UIKit.UIImage, fileName: Swift.String, mimeType: GBGCore.MimeType)
}
public struct GBGComment : ObjectMapper.Mappable, Swift.Equatable {
  public var text: Swift.String?
  public var code: Swift.Int?
  public var override: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGComment, b: GBGCore.GBGComment) -> Swift.Bool
}
public struct GBGAnalysisItem : ObjectMapper.Mappable, Swift.Equatable {
  public var text: Swift.String?
  public var code: Swift.Int?
  public var override: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGAnalysisItem, b: GBGCore.GBGAnalysisItem) -> Swift.Bool
}
public struct GBGTriangulation : ObjectMapper.Mappable, Swift.Equatable {
  public var candidates: [GBGCore.GBGTriangulationCandidate]?
  public var analysis: [GBGCore.GBGTriangulationAnalysis]?
  public var rule: GBGCore.GBGTriangulationRule?
  public var result: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGTriangulation, b: GBGCore.GBGTriangulation) -> Swift.Bool
}
public struct GBGFlags : ObjectMapper.Mappable, Swift.Equatable {
  public var address: Swift.String?
  public var firstName: Swift.String?
  public var lastName: Swift.String?
  public var birthDate: Swift.String?
  public var alert: Swift.String?
  public var pass: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGFlags, b: GBGCore.GBGFlags) -> Swift.Bool
}
@_hasMissingDesignatedInitializers open class GBGVerifyResponseManager {
  public static let shared: GBGCore.GBGVerifyResponseManager
  public var requestURL: Swift.String?
  public var verificationId: Swift.String?
  public var documentId: Swift.String?
  public var personId: Swift.String?
  @objc deinit
}
public struct GBGTriangulationRule : ObjectMapper.Mappable, Swift.Equatable {
  public var id: Swift.String?
  public var desc: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGTriangulationRule, b: GBGCore.GBGTriangulationRule) -> Swift.Bool
}
public struct GBGDocumentDataRaw : ObjectMapper.Mappable, Swift.Equatable {
  public var fields: [GBGCore.GBGField]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDocumentDataRaw, b: GBGCore.GBGDocumentDataRaw) -> Swift.Bool
}
public struct GBGPhone : ObjectMapper.Mappable, Swift.Equatable {
  public var type: Swift.String?
  public var number: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGPhone, b: GBGCore.GBGPhone) -> Swift.Bool
}
open class GBGBaseService<Target> : GBGCore.AuthTokenManagable where Target : Moya.TargetType {
  public init()
  public init(plugins: [Moya.PluginType] = [], timeout: Foundation.TimeInterval = 20, cachePolicy: Foundation.NSURLRequest.CachePolicy = .useProtocolCachePolicy)
  public func setPlugins(_ plugins: [Moya.PluginType])
  public func getProvider() -> Moya.MoyaProvider<Target>
  public func setAuthToken()
  public func execute<ResponseType>(_ target: Target, _ reponseType: ResponseType?, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<ResponseType>>?, failure: GBGCore.GBGFailureResponseHandler?) where ResponseType : ObjectMapper.Mappable
  public func executeToArray<ResponseType>(_ target: Target, _ reponseType: ResponseType?, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<[ResponseType]>>?, failure: GBGCore.GBGFailureResponseHandler?) where ResponseType : ObjectMapper.Mappable
  @objc deinit
}
public enum GBGAuthKeychainConstants : Swift.String {
  case account
  case service
  case accessToken
  case expiry
  case tokenType
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
public struct GBGNetworkModel {
  public let base: Swift.String?
  public let path: Swift.String?
  public let headers: [Swift.String : Swift.String]?
  public let method: Moya.Method
  public init(base: Swift.String?, path: Swift.String?, headers: [Swift.String : Swift.String]?, method: Moya.Method)
}
public protocol AuthTokenManagable {
  var accessToken: Swift.String? { get }
}
extension AuthTokenManagable {
  public var accessToken: Swift.String? {
    get
  }
}
public struct GBGVerificationResultResponse : ObjectMapper.Mappable, Swift.Equatable {
  public var verificationId: Swift.String?
  public var timestamp: Swift.String?
  public var verificationURL: Swift.String?
  public var score: Swift.Int?
  public var decision: GBGCore.GBGVerificationDecision?
  public var documentData: GBGCore.GBGDocumentData?
  public var action: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGVerificationResultResponse, b: GBGCore.GBGVerificationResultResponse) -> Swift.Bool
}
public struct GBGResultResponse : ObjectMapper.Mappable, Swift.Equatable {
  public var function: Swift.String?
  public var state: Swift.String?
  public var score: Swift.Int?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGResultResponse, b: GBGCore.GBGResultResponse) -> Swift.Bool
}
public struct GBGTriangulationCandidate : ObjectMapper.Mappable, Swift.Equatable {
  public var index: Swift.String?
  public var title: Swift.String?
  public var firstName: Swift.String?
  public var middlenames: [Swift.String]?
  public var lastnames: [Swift.String]?
  public var gender: Swift.String?
  public var birthdate: Swift.String?
  public var addresses: [GBGCore.GBGCandidateAddress]?
  public var phones: [GBGCore.GBGPhone]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGTriangulationCandidate, b: GBGCore.GBGTriangulationCandidate) -> Swift.Bool
}
open class GBGAuthCredentials : GBGCore.Buildable {
  public var grantType: Swift.String
  public var clientId: Swift.String
  public var clientSecret: Swift.String
  public var scope: Swift.String
  public var accessToken: Swift.String
  required public init()
  @discardableResult
  public func withGrantType(_ value: Swift.String) -> GBGCore.GBGAuthCredentials
  @discardableResult
  public func withClientId(_ value: Swift.String) -> GBGCore.GBGAuthCredentials
  @discardableResult
  public func withClientSecret(_ value: Swift.String) -> GBGCore.GBGAuthCredentials
  @discardableResult
  public func withScope(_ value: Swift.String) -> GBGCore.GBGAuthCredentials
  @discardableResult
  public func withAccessToken(_ value: Swift.String) -> GBGCore.GBGAuthCredentials
  @objc deinit
}
open class GBGOAuthCredentials : GBGCore.Buildable {
  public var grantType: Swift.String
  public var username: Swift.String
  public var password: Swift.String
  public var clientSecret: Swift.String
  public var clientId: Swift.String
  public var scope: Swift.String
  public var resource: Swift.String
  required public init()
  @objc deinit
}
extension GBGOAuthCredentials {
  @discardableResult
  public func withGrantType(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withUsername(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withPassword(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withClientSecret(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withClientId(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withScope(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
  @discardableResult
  public func withResource(_ value: Swift.String) -> GBGCore.GBGOAuthCredentials
}
public struct GBGDecision : ObjectMapper.Mappable, Swift.Equatable {
  public var id: Swift.String?
  public var decision: Swift.String?
  public var state: Swift.String?
  public var range: GBGCore.GBGRange?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDecision, b: GBGCore.GBGDecision) -> Swift.Bool
}
public enum MimeType : Swift.String {
  case jpg
  case png
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
public enum DocumentType : Swift.String {
  case passport
  case drivingLicense
  case europeanID
  case australianMedicare
  case chineseID
  case other
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
public struct GBGResident : ObjectMapper.Mappable, Swift.Equatable {
  public var from: Swift.String?
  public var to: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGResident, b: GBGCore.GBGResident) -> Swift.Bool
}
public enum GBGAuthAPI {
  case authenticate(network: GBGCore.GBGNetworkModel, credentialsManager: GBGCore.GBGAuthCredentialsManager)
}
extension GBGAuthAPI : Moya.TargetType, GBGCore.TaskManagable {
  public var baseURL: Foundation.URL {
    get
  }
  public var headers: [Swift.String : Swift.String]? {
    get
  }
  public var path: Swift.String {
    get
  }
  public var method: Moya.Method {
    get
  }
  public var sampleData: Foundation.Data {
    get
  }
  public var task: Moya.Task {
    get
  }
}
public struct GBGCountry : ObjectMapper.Mappable, Swift.Equatable {
  public var code: Swift.String?
  public var name: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGCountry, b: GBGCore.GBGCountry) -> Swift.Bool
}
public struct GBGDocument {
  public let data: Foundation.Data?
  public let fileName: Swift.String!
  public let fileExtension: Swift.String!
  public let mimeType: GBGCore.MimeType!
  public let documentFace: GBGCore.DocumentFace!
  public let documentType: GBGCore.DocumentType!
  public init(image: UIKit.UIImage, fileName: Swift.String, mimeType: GBGCore.MimeType, documentFace: GBGCore.DocumentFace, documentType: GBGCore.DocumentType)
}
public struct GBGVerificationResponse : ObjectMapper.Mappable, Swift.Equatable {
  public var timestamp: Swift.String?
  public var customerReference: Swift.String?
  public var action: Swift.String?
  public var verificationId: Swift.String?
  public var combined: GBGCore.GBGCombined?
  public var verification: GBGCore.GBGVerification?
  public var journey: GBGCore.GBGJourney?
  public var triangulation: GBGCore.GBGTriangulation?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGVerificationResponse, b: GBGCore.GBGVerificationResponse) -> Swift.Bool
}
public struct GBGCombined : ObjectMapper.Mappable, Swift.Equatable {
  public var state: Swift.String?
  public var score: Swift.Int?
  public var decision: Swift.String?
  public var verifications: [GBGCore.GBGResult]?
  public var analysis: GBGCore.GBGCombinedAnalysis?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGCombined, b: GBGCore.GBGCombined) -> Swift.Bool
}
open class GBGAuthService : GBGCore.GBGBaseService<GBGCore.GBGAuthAPI> {
  override public init()
  public func authenticate(_ authCredentials: GBGCore.GBGAuthCredentials, success: GBGCore.GBGSuccessResponseHandler<GBGCore.GBGResponse<GBGCore.GBGAuthResponse>>?, failure: GBGCore.GBGFailureResponseHandler?)
  @objc deinit
  override public init(plugins: [Moya.PluginType] = super, timeout: Foundation.TimeInterval = super, cachePolicy: Foundation.NSURLRequest.CachePolicy = super)
}
public struct GBGDocumentData : ObjectMapper.Mappable, Swift.Equatable {
  public var raw: GBGCore.GBGDocumentDataRaw?
  public var person: GBGCore.GBGVerificationPerson?
  public var typed: GBGCore.GBGDocumentDataTyped?
  public var images: [GBGCore.GBGDocumentImage]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDocumentData, b: GBGCore.GBGDocumentData) -> Swift.Bool
}
public struct GBGLocation : ObjectMapper.Mappable, Swift.Equatable {
  public var latitude: Swift.String?
  public var longitude: Swift.String?
  public var geoAccuracy: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGLocation, b: GBGCore.GBGLocation) -> Swift.Bool
}
public struct GBGRange : ObjectMapper.Mappable, Swift.Equatable {
  public var start: Swift.Int?
  public var end: Swift.Int?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGRange, b: GBGCore.GBGRange) -> Swift.Bool
}
public struct GBGClassification : ObjectMapper.Mappable, Swift.Equatable {
  public var type: Swift.String?
  public var category: Swift.String?
  public var name: Swift.String?
  public var issue: Swift.String?
  public var issuingAuthority: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGClassification, b: GBGCore.GBGClassification) -> Swift.Bool
}
public struct GBGCleansed : ObjectMapper.Mappable, Swift.Equatable {
  public var lines: [Swift.String]?
  public var address: Swift.String?
  public var premise: Swift.String?
  public var building: Swift.String?
  public var subBuilding: Swift.String?
  public var thoroughfare: Swift.String?
  public var dependentThoroughfare: Swift.String?
  public var locality: Swift.String?
  public var dependentLocality: Swift.String?
  public var doubleDependentLocality: Swift.String?
  public var postalCode: Swift.String?
  public var postBox: Swift.String?
  public var country: Swift.String?
  public var superAdministrativeArea: Swift.String?
  public var administrativeArea: Swift.String?
  public var subAdministrativeArea: Swift.String?
  public var organization: Swift.String?
  public var quality: GBGCore.GBGQuality?
  public var location: GBGCore.GBGLocation?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGCleansed, b: GBGCore.GBGCleansed) -> Swift.Bool
}
public struct GBGDeuIDCard : ObjectMapper.Mappable, Swift.Equatable {
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDeuIDCard, b: GBGCore.GBGDeuIDCard) -> Swift.Bool
}
public struct Front : ObjectMapper.Mappable, Swift.Equatable {
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.Front, b: GBGCore.Front) -> Swift.Bool
}
public struct Back : ObjectMapper.Mappable, Swift.Equatable {
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.Back, b: GBGCore.Back) -> Swift.Bool
}
public class GBGCore : GBGCore.ModuleItem {
  required public init()
  @objc deinit
}
public struct GBGVerificationDecision : ObjectMapper.Mappable, Swift.Equatable {
  public var current: Swift.String?
  public var combined: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGVerificationDecision, b: GBGCore.GBGVerificationDecision) -> Swift.Bool
}
public struct GBGAnalysis : ObjectMapper.Mappable, Swift.Equatable {
  public var name: Swift.String?
  public var desc: Swift.String?
  public var id: Swift.Int?
  public var country: GBGCore.GBGCountry?
  public var comments: [GBGCore.GBGAnalysisItem]?
  public var matches: [GBGCore.GBGAnalysisItem]?
  public var warnings: [GBGCore.GBGAnalysisItem]?
  public var mismatches: [GBGCore.GBGAnalysisItem]?
  public var flags: GBGCore.GBGFlag?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGAnalysis, b: GBGCore.GBGAnalysis) -> Swift.Bool
}
extension Date {
  public var iso8601: Swift.String {
    get
  }
}
public protocol ModuleItem {
  @discardableResult
  init()
}
public struct GBGOAuthResponse : ObjectMapper.Mappable {
  public var tokenType: Swift.String?
  public var scope: Swift.String?
  public var expiresIn: Swift.String?
  public var extExpiresIn: Swift.String?
  public var expiresOn: Swift.String?
  public var notBefore: Swift.String?
  public var resource: Swift.String?
  public var accessToken: Swift.String?
  public var refreshToken: Swift.String?
  public var idToken: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
}
public enum GBGOAuthAPI {
  case authorize(network: GBGCore.GBGNetworkModel, credentialsManager: GBGCore.GBGOAuthCredentialsManager)
}
extension GBGOAuthAPI : Moya.TargetType, GBGCore.TaskManagable {
  public var baseURL: Foundation.URL {
    get
  }
  public var headers: [Swift.String : Swift.String]? {
    get
  }
  public var path: Swift.String {
    get
  }
  public var method: Moya.Method {
    get
  }
  public var sampleData: Foundation.Data {
    get
  }
  public var task: Moya.Task {
    get
  }
}
public struct GBGAddress : ObjectMapper.Mappable, Swift.Equatable {
  public var extracted: Swift.String?
  public var enrichment: GBGCore.GBGEnrichment?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGAddress, b: GBGCore.GBGAddress) -> Swift.Bool
}
public struct GBGCandidateAddress : ObjectMapper.Mappable, Swift.Equatable {
  public var type: Swift.String?
  public var address: GBGCore.GBGCleansed?
  public var enrichments: GBGCore.GBGEnrichment?
  public var resident: GBGCore.GBGResident?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGCandidateAddress, b: GBGCore.GBGCandidateAddress) -> Swift.Bool
}
public struct GBGErrorResponse<T> : Swift.Error {
  public let response: Moya.Response?
  public let body: T?
  public init(response: Moya.Response?, body: T?)
}
public struct GBGAuthResponse : ObjectMapper.Mappable {
  public var accessToken: Swift.String?
  public var expiresIn: Swift.Int?
  public var tokenType: Swift.String?
  public init(accessToken: Swift.String?, expiresIn: Swift.Int?, tokenType: Swift.String?)
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
}
@_hasMissingDesignatedInitializers public class GBGAuthCredentialsManager {
  @objc deinit
}
public struct GBGTriangulationAnalysis : ObjectMapper.Mappable, Swift.Equatable {
  public var comments: [GBGCore.GBGAnalysisItem]?
  public var matches: [GBGCore.GBGAnalysisItem]?
  public var warnings: [GBGCore.GBGAnalysisItem]?
  public var mismatches: [GBGCore.GBGAnalysisItem]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGTriangulationAnalysis, b: GBGCore.GBGTriangulationAnalysis) -> Swift.Bool
}
public struct GBGLink : ObjectMapper.Mappable, Swift.Equatable {
  public var href: Swift.String?
  public var rel: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGLink, b: GBGCore.GBGLink) -> Swift.Bool
}
public struct GBGVerificationPerson : ObjectMapper.Mappable, Swift.Equatable {
  public var title: Swift.String?
  public var firstName: Swift.String?
  public var middlenames: [Swift.String]?
  public var lastnames: [Swift.String]?
  public var gender: Swift.String?
  public var birthdate: Swift.String?
  public var addresses: [GBGCore.GBGCandidateAddress]?
  public var phones: [GBGCore.GBGPhone]?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGVerificationPerson, b: GBGCore.GBGVerificationPerson) -> Swift.Bool
}
public struct GBGDocumentType : ObjectMapper.Mappable {
  public var documentName: Swift.String?
  public var journeyIDs: [GBGCore.GBGDocumentTypeJourney]?
  public var schema: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
}
public struct GBGJourney : ObjectMapper.Mappable, Swift.Equatable {
  public var journeyId: Swift.String?
  public var journeyVersion: Swift.Int?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGJourney, b: GBGCore.GBGJourney) -> Swift.Bool
}
public struct GBGQuality : ObjectMapper.Mappable, Swift.Equatable {
  public var avc: Swift.String?
  public var aqi: Swift.String?
  public init(avc: Swift.String?, aqi: Swift.String?)
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGQuality, b: GBGCore.GBGQuality) -> Swift.Bool
}
public struct GBGFlag : ObjectMapper.Mappable, Swift.Equatable {
  public var address: Swift.String?
  public var firstName: Swift.String?
  public var lastName: Swift.String?
  public var birthDate: Swift.String?
  public var alert: Swift.String?
  public var pass: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGFlag, b: GBGCore.GBGFlag) -> Swift.Bool
}
public struct GBGDerivedData : ObjectMapper.Mappable, Swift.Equatable {
  public var isRecognised: Swift.Bool?
  public var person: GBGCore.GBGPerson?
  public var fields: [GBGCore.GBGField]?
  public var images: [GBGCore.GBGImageResponse]?
  public var classification: GBGCore.GBGClassification?
  public var gbrDrivingLicense: GBGCore.GBGGbrDrivingLicence?
  public var deuIDCard: GBGCore.GBGDeuIDCard?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDerivedData, b: GBGCore.GBGDerivedData) -> Swift.Bool
}
public struct GBGDocumentImage : ObjectMapper.Mappable, Swift.Equatable {
  public var imageType: Swift.String?
  public var imageID: Swift.String?
  public var imageURL: Swift.String?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDocumentImage, b: GBGCore.GBGDocumentImage) -> Swift.Bool
}
public struct GBGDocumentDataTyped : ObjectMapper.Mappable, Swift.Equatable {
  public var ukDrivingLicense: GBGCore.GBGUKDrivingLicence?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGDocumentDataTyped, b: GBGCore.GBGDocumentDataTyped) -> Swift.Bool
}
public protocol Buildable {
  init()
}
public struct GBGResult : ObjectMapper.Mappable, Swift.Equatable {
  public var result: GBGCore.GBGResultResponse?
  public init?(map: ObjectMapper.Map)
  public mutating func mapping(map: ObjectMapper.Map)
  public static func == (a: GBGCore.GBGResult, b: GBGCore.GBGResult) -> Swift.Bool
}
public enum DocumentFace : Swift.String {
  case front
  case back
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
extension GBGCore.GBGAuthKeychainConstants : Swift.Equatable {}
extension GBGCore.GBGAuthKeychainConstants : Swift.Hashable {}
extension GBGCore.GBGAuthKeychainConstants : Swift.RawRepresentable {}
extension GBGCore.MimeType : Swift.Equatable {}
extension GBGCore.MimeType : Swift.Hashable {}
extension GBGCore.MimeType : Swift.RawRepresentable {}
extension GBGCore.DocumentType : Swift.Equatable {}
extension GBGCore.DocumentType : Swift.Hashable {}
extension GBGCore.DocumentType : Swift.RawRepresentable {}
extension GBGCore.DocumentFace : Swift.Equatable {}
extension GBGCore.DocumentFace : Swift.Hashable {}
extension GBGCore.DocumentFace : Swift.RawRepresentable {}
