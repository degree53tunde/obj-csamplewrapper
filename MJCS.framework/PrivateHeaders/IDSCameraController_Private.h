//
//  IDSCameraController_Private.h
//  IDSCameraController
//
//  Created by Edvardas Maslauskas on 04/08/2016.
//  Copyright © 2016 IDScan Biometrics Ltd. All rights reserved.
//

#import "IDSCameraController.h"
@class IDSCameraController;

/**
 *  This is where you can choose the camera overlays.
 */
typedef NS_ENUM(NSInteger, IDSCameraTemplate) {
    IDSCameraTemplateID = 0,
    IDSCameraTemplateUtility,
    IDSCameraTemplateSelfie
};

@interface IDSCameraController ()

/**
 *  UIImage image for the watermark
 */
@property (nonatomic, strong, nullable) UIImage *watermarkImage;

/**
 *  @brief When enabled, the camera resolution will default to the maximum possible resolution for the device if it is available, including 4K
 *
 *  @warning IDES runs exponentially slower when processing 4K images 
 */
@property (nonatomic, assign) BOOL allowsHighResolution;

/**
 *  @brief When enabled, the camera view will be blur totally
 */
@property (nonatomic, assign) BOOL frameCaptureDisabled;

/**
 Allows to provide static image when camera is on back side which will be callbacked as camera preview
 frame and still image capture result on simulator when read camera
 hardware is not available.
 */
@property (nonatomic, strong, nullable) UIImage *simulatorBackCameraOutput;

/**
 Allows to provide static image when camera is on front side which will be callbacked as camera preview
 frame and still image capture result on simulator when read camera
 hardware is not available.
 */
@property (nonatomic, strong, nullable) UIImage *simulatorFrontCameraOutput;

/**
 *  Draw polygon at specified keypoints
 *
 *  @param keypoints NSArray that contains NSValue objects of lines' keypoints
 *  @param lineColor UIColor of the polygon's line color
 */
- (void)drawPolygonAtKeyPoints:(nonnull NSArray<NSValue *> *)keypoints andLineColor:(nonnull UIColor *)lineColor;

/**
 *  Method that presents camera controller with specified size overlay
 *
 *  @param fromViewController UIVIewController from which IDSCameraController is going to be presented
 *  @param frame CGRect of camera controller in the parent UIViewController
 *  @warning Use this method when you want to present IDSCameraController as overlay in it's parent controller
 */
- (void)overlayCameraFromViewController:(UIViewController *_Nonnull)fromViewController withFrame:(CGRect)frame;

/**
 *  Method that dismisses camera controller from it's parent controller
 *
 *  @param completion Triggered once IDSCameraController is removed from it's parent UIViewController
 *  @warning Use this method only when IDSCameraController was presented as overlay
 */
- (void)dismissCameraOverlayWithCompletion:(void (^__nullable)(void))completion;

/**
 *  Shows an alert box in the view
 *
 *  @param userInfo where dictionary should be passed with information
 *  @warning
 *  UserInfo params:
 *  title - NSString of the title message
 *  message - NSString of the body message
 *  dismissTime - NSNumber after which alert view should be dismissed
 *  useVoice - NSNumber that resolves to BOOL which determines whether or not the message and title will be spoken aloud
 *  == 0 alert won't dissapear automatically
 *  > 0 alert will disappear after specified time
 *  @note if dismissTime is not set the default value will be 7s
 */
- (void)showAlertViewWithInfo:(nonnull NSDictionary*)userInfo;

/**
 *  Dismiss alert box from the view
 */
- (void)dismissAlertView;

/**
 *  Changes trigget button hidden state
 *
 *  @param hidden BOOL representing whether trigger button should be hidden or not
 */
- (void)toggleTriggerButtonHidden:(BOOL)hidden;

/**
 *  Freezes camera with the last frame and adds blur effect on it
 */
- (void)freezeCamera;

/**
 *  Resumes camera and removes blur effect from the view
 */
- (void)resumeCamera;

/**
 *  Hide all camera elements
 *
 * @param hidden BOOL representing whether camera elements should be hidden or not
 */
- (void)hideCameraElements:(BOOL)hidden;

/**
 *  Change device position
 *
 * @param position AVCaptureDevicePosition representing camera position
 */
- (void)changeCameraDevicePosition:(AVCaptureDevicePosition)position;

/**
 *  show default templates for ID, Utility and Selfie screens
 *
 * @param template IDSCameraTemplate for choosing the type
 */
- (void)showDefaultTemplate:(IDSCameraTemplate)template;

/**
 Hide default template from the view
 */
- (void)dismissDefaultTemplate;

@property (nonatomic, nonnull) UIImageView *watermarkImageView;

@property (nonatomic, strong, nullable) UIView *templateView;

/// Hides help button on the camera screen
/// @param hidden value whether button should be hidden
- (void)setHelpButtonHidden:(BOOL)hidden;

@end
