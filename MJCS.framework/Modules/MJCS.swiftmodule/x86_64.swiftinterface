// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2 (swiftlang-1103.0.32.1 clang-1103.0.32.29)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MJCS
import AFNetworking
import AudioToolbox
import FLAnimatedImage
import Foundation
@_exported import MJCS
import Swift
import UIKit
@_inheritsConvenienceInitializers @IBDesignable @objc public class IDSSpinnerView : UIKit.UIView {
  @objc override dynamic public var layer: QuartzCore.CAShapeLayer {
    @objc get
  }
  @objc override dynamic public class var layerClass: Swift.AnyClass {
    @objc get
  }
  @IBInspectable @objc public var strokeColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc override dynamic public func layoutSubviews()
  @objc override dynamic public func didMoveToWindow()
  @objc override dynamic public func awakeFromNib()
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
public enum IDSError : Swift.Error, Foundation.LocalizedError {
  case invalidConfiguration(message: Swift.String)
  case NFCDeviceNotSupported
  public var failureReason: Swift.String {
    get
  }
}
@objc(IDSError) public enum _ObjCIDSError : Swift.Int {
  case invalidConfiguration
  case NFCDeviceNotSupported
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
public enum IDSFailure : Swift.Error, Foundation.LocalizedError {
  case sessionTimeout
  case sessionError
  case connectionSecurityError
  case userCancelled
  case networkError(statusCode: Swift.Int, errorDescription: Swift.String)
  case internalFailure(reason: Swift.String, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  case nfcError
  public var failureReason: Swift.String? {
    get
  }
}
@_hasMissingDesignatedInitializers @objc(IDSFailure) @objcMembers final public class _ObjCIDSFailure : ObjectiveC.NSObject {
  @objc final public var reason: _ObjCIDSFailureReason {
    @objc get
  }
  @objc final public var error: Swift.Error? {
    @objc get
  }
  @objc deinit
  @objc override dynamic public init()
}
@objc(IDSFailureReason) public enum _ObjCIDSFailureReason : Swift.Int {
  case sessionTimeout
  case sessionError
  case connectionSecurityError
  case userCancelled
  case networkError
  case internalFailure
  case nfcError
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc extension NSString {
  @objc dynamic public func localized(oldKey: Foundation.NSString?, oldTableName: Foundation.NSString?) -> Foundation.NSString
}
extension UILabel {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UIButton {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UINavigationItem {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UIBarItem {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UISegmentedControl {
  @objc @IBInspectable dynamic public var xibLocKeys: Swift.String? {
    @objc get
    @objc set(keys)
  }
}
extension UITextField {
  @objc @IBInspectable dynamic public var xibPlaceholderLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSGlobalConfig : ObjectiveC.NSObject {
  @objc public static func builder() -> IDSGlobalConfig
  @objc @discardableResult
  public func withCustomLocalization() -> IDSGlobalConfig
  @objc @discardableResult
  public func withCustomLocalization(tableName: Swift.String, inBundle: Foundation.Bundle?) -> IDSGlobalConfig
  @objc deinit
  @objc override dynamic public init()
}
extension UIView {
  @discardableResult
  public func fillSuperView(_ edges: UIKit.UIEdgeInsets = UIEdgeInsets.zero) -> [UIKit.NSLayoutConstraint]
  @discardableResult
  public func addLeadingConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .leading, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addTrailingConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .trailing, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addLeftConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .left, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addRightConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .right, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addTopConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .top, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addBottomConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .bottom, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addCenterXConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addCenterYConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addWidthConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addHeightConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
}
public enum IDSNetworkingError : Swift.Int {
  case unknown
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
extension Error {
  public func getNetworkingErrorDesc() -> Swift.String
  public func getNetwrokingErrorCode() -> Swift.Int
  public func getNormalisedError() -> Swift.Error
}
@objc extension NSError {
  @objc dynamic public func getNetworkingErrorDesc() -> Swift.String
  @objc dynamic public func getNetwrokingErrorCode() -> Swift.Int
  @objc dynamic public func getNormalisedError() -> Swift.Error
}
@objc public enum IDSDocumentScannerType : Swift.Int {
  case document = 0
  case utility = 1
  case selfie = 2
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSDocumentScannerImgQualityConfig : ObjectiveC.NSObject {
  @objc public var low_resolution_check_enabled: Swift.Bool {
    get
  }
  @objc public var blur_check_enabled: Swift.Bool {
    get
  }
  @objc public var glare_check_enabled: Swift.Bool {
    get
  }
  @objc public var boundary_check_enabled: Swift.Bool {
    get
  }
  @objc public static func builder() -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withLowResolutionCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withGlareCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withBlurCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withBoundaryCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc deinit
  @objc override dynamic public init()
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSDocumentScannerConfig : ObjectiveC.NSObject {
  @objc public var manualCaptureDisabled: Swift.Bool {
    get
  }
  @objc public var enable4k: Swift.Bool {
    get
  }
  @objc public var scannerType: IDSDocumentScannerType {
    get
  }
  @objc public var cropDocumentImages: Swift.Bool {
    get
  }
  @objc public var captureButtonAppearTime: Swift.Double {
    get
  }
  @objc public var ImgQualityConfig: IDSDocumentScannerImgQualityConfig {
    get
  }
  @objc public static func builder() -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withManualCaptureDisabled(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func with4KCaptureEnabled(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withScannerType(_ value: IDSDocumentScannerType) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withUsingCroppedDocumentImages(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withCaptureButtonAppearTime(_ value: Swift.Double) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withImgQualityConfig(_ value: IDSDocumentScannerImgQualityConfig) -> IDSDocumentScannerConfig
  @objc deinit
  @objc override dynamic public init()
}
@objc public protocol IDSLivenessOverlayViewControllerProtocol {
  @objc func readyForLiveness()
}
@_inheritsConvenienceInitializers @objc(IDSLivenessOverlayViewController) public class IDSLivenessOverlayViewController : UIKit.UIViewController {
  @objc weak public var delegate: IDSLivenessOverlayViewControllerProtocol?
  @objc override dynamic public func viewDidAppear(_ animated: Swift.Bool)
  @objc deinit
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
extension IDSLivenessOverlayViewController : IDSLivenessViewProtocol {
  @objc dynamic public func requestedCalibration(_ calibration: IDSLivenessCalibration)
  @objc dynamic public func livenessLoadingFinished()
  @objc dynamic public func livenessFinished()
  @objc dynamic public func requestedAction(_ action: Swift.Int)
}
@objc @objcMembers public class IDSLivenessSubmissionHandler : ObjectiveC.NSObject {
  public typealias ResponseHandler = (Swift.Error?, IDSEnterpriseResponse?) -> Swift.Void
  @objc public init(credentials: IDSEnterpriseCredentials, journeyID: Swift.String, responseHandler: @escaping IDSLivenessSubmissionHandler.ResponseHandler)
  @objc public func onFace(image: UIKit.UIImage)
  @objc public func onLiveness(result: Any)
  @objc deinit
  @objc override dynamic public init()
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSCustomerJourneyConfig : ObjectiveC.NSObject {
  @objc public var documentScannerConfig: IDSDocumentScannerConfig {
    get
  }
  @objc public static func builder() -> IDSCustomerJourneyConfig
  @objc @discardableResult
  public func withDocumentScannerConfig(_ value: IDSDocumentScannerConfig) -> IDSCustomerJourneyConfig
  @objc deinit
  @objc override dynamic public init()
}
extension _ObjCIDSError : Swift.Equatable {}
extension _ObjCIDSError : Swift.Hashable {}
extension _ObjCIDSError : Swift.RawRepresentable {}
extension _ObjCIDSFailureReason : Swift.Equatable {}
extension _ObjCIDSFailureReason : Swift.Hashable {}
extension _ObjCIDSFailureReason : Swift.RawRepresentable {}
extension IDSNetworkingError : Swift.Equatable {}
extension IDSNetworkingError : Swift.Hashable {}
extension IDSNetworkingError : Swift.RawRepresentable {}
extension IDSDocumentScannerType : Swift.Equatable {}
extension IDSDocumentScannerType : Swift.Hashable {}
extension IDSDocumentScannerType : Swift.RawRepresentable {}
