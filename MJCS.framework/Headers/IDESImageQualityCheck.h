//
//  IDESImageQualityCheck.h
//  idesv2
//
//  Created by Edvardas Maslauskas on 12/09/2018.
//  Copyright © 2018 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Returns status of the image quality check. For detailed information about
 * check status `[IDESImageQualityCheck statusDescription]` value.
 * @return status of the image quality check.
 */
typedef NS_ENUM(NSInteger, IDESImageQualityCheckStatus) {
    /**
     *  If status is undetermined.
     */
    IDESImageQualityCheckStatusUndetermined = 0,
    /**
     *  If image quality is passing certain criterias.
     */
    IDESImageQualityCheckStatusGood = 1,
    /**
     *  If image quality is failing certain criterias
     */
    IDESImageQualityCheckStatusBad = 2,

};

/**
 *  Object which explains a particular image quality check performed on a returned document
 */
@interface IDESImageQualityCheck : NSObject

/**
 * Returns a value representing identifier of the image quality check.
 * This identifier should be used to distiguish the check and refer to it
 * programatically if needed.
 * @return image quality check's id.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Returns a value specifying description of the image quality check.
 * @return description of image quality check.
 */
@property (nonatomic, readonly, nonnull) NSString *checkDescription;

/**
 * Returns status of the image quality check.
 * @return status of image quality check.
 */
@property (nonatomic, assign, readonly) IDESImageQualityCheckStatus status;

/**
 * Returns description of the image quality check status.
 * @return description of image quality check status.
 */
@property (nonatomic, readonly, nullable) NSString *statusDescription;

@end
