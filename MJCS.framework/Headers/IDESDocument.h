//
//  IDESDocument.h
//  idesv2
//
//  Created by David Okun on 06/07/2016.
//  Copyright © 2016 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "IDESAuthenticationCheck.h"
#import "IDESExtractedField.h"
#import "IDESImageQualityCheck.h"

/**
 * Provides access to the high level overall result of the authentication
 * on the document. Possible values are:
 * passed: the document passed the authentication checks with an overall
 * successful result;
 * failed: the document failed many of the authentication checks that pose
 * a concern on document state;
 * undetermined: the state of the checks was inconclusive this might be
 * because not enough;
 * Authentication checks were carried on the document or the checks returned
 * an intermediate result.
 */
typedef NS_ENUM(NSInteger, IDESAuthenticationState) {
    /**
     *  Cannot decide whether validation passed or failed
     */
    IDESAuthenticationStateUndetermined = 0,
    /**
     *  Document is accepted as a valid document
     */
    IDESAuthenticationStatePassed = 1,
    /**
     *  Document does not meet IDscan criteria for a valid document
     */
    IDESAuthenticationStateFailed = 2
};

/**
 * Internal processing value
 */
typedef NS_ENUM(NSInteger, IDESAction) {
    
    IDESActionDone = 0,

    IDESActionNeedMoreFrames = 1,
};

/**
 *  Object which represents a document as returned from the `IDESDocumentProcessingService`
 */
@interface IDESDocument : NSObject

/**
 * Returns boolean whether scanned document contains bad fields due to the
 * glare, blur or other factors
 * @return if document contains bad fields
 */
@property (nonatomic, readonly, assign) BOOL hasBadFields;

/**
 * Returns a unique document type identifier which is a code that uniquely
 * identifies this specific document type.
 * @return unique identifier of the document type.
 */
@property (nonatomic, readonly, nullable) NSString *typeId;

/**
 * Returns document name which is a friendly name that represents the issuer
 * and type or subtype of the document (e.g. 'United Kingdom Provisional
 * Driving License').
 * @return name of the document.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * Returns the document type high level category (e.g. 'Identification Card'
 * which covers both National and Foreigner Identifcation Cards types). This
 * is the top level of document type hierarchy
 * @return category of the document.
 */
@property (nonatomic, readonly, nullable) NSString *category;

/**
 * Returns document type (e.g. 'National Identification Card'). This is the
 * second level of document type hierarchy and is usually the most used
 * identifier for distinguishing document types.
 * @return type of the document.
 */
@property (nonatomic, readonly, nullable) NSString *type;

/**
 * Returns the document subtype. Subtype is the third level of document type
 * hierarchy and it identies the document more specifically and usually
 * differs from one issuer to another (E.g. 'Full Driving Licence',
 * 'Provisional Driving Licence', 'Proabationary Driving Licence' etc.).
 * @return subtype of the document.
 */
@property (nonatomic, readonly, nullable) NSString *subtype;

/**
 * Returns document issue which distinguishes different issues (versions)
 * of the same document type or subtype. Document issue can be a year
 * identifying the year of issue or a propietary code representation this
 * issue is known with.
 * @return issue of the document.
 */
@property (nonatomic, readonly, nullable) NSString *issue;

/**
 * Returns document issuing country code. This field uses standard three
 * letter ISO 3166-1 country codes (e.g. 'GBR' for United Kingdom).
 * @return document issuing country code.
 */
@property (nonatomic, readonly, nullable) NSString *issuingCountryCode;

/**
 * Returns document issuing country name (E.g. United Kingdom).
 * @return document issuing country name.
 */
@property (nonatomic, readonly, nullable) NSString *issuingCountryName;

/**
 * Returns document issuing state code as it is recognized in the issuing
 * country. This field should always be used along with issuing_country_code
 * field. (e.g 'WA' for either Western Australia in Australia or Washington
 * in United States of America).
 * @return document issuing state code.
 */
@property (nonatomic, readonly, nullable) NSString *issuingStateCode;

/**
 * Returns document issuing state name (E.g. Washington).
 * @return document issuing state name.
 */
@property (nonatomic, readonly, nullable) NSString *issuingStateName;

/**
 * Provides access to the high level overall result of the authentication
 * on the document. Possible values are:
 * passed: the document passed the authentication checks with an overall
 * successful result;
 * failed: the document failed many of the authentication checks that pose
 * a concern on document state;
 * undetermined: the state of the checks was inconclusive this might be
 * because not enough;
 * Authentication checks were carried on the document or the checks returned
 * an intermediate result.
 * @return document authentication state.
 */
@property (nonatomic, readonly) IDESAuthenticationState authenticationState;

/**
 * Provides access to the information that were extracted from the document
 * (e.g. FirstName, LastName and BirthDate of the document holder,
 * ExpiryDate of the document). This map is indexed by the field names
 * (which follow IDES field naming standard).
 * @return NSDictionary of information extracted from the document.
 */
@property (nonatomic, readonly, nullable) NSDictionary<NSString *, IDESExtractedField *> *extractedFields;

/**
 * Provides access to the details of the authentication checks that were
 * processed against the document. This map is indexed by the authentication
 * check ids (which are part of IDES authentication checks list).
 * @return NSDictionary of authentication checks performed on the document.
 */
@property (nonatomic, readonly, nullable) NSDictionary<NSString *, IDESAuthenticationCheck *> *authenticationChecks;

/**
 * Get image quality check results on input document .
 * @return NSDictionary expose quality check results.
 */
@property (nonatomic, readonly, nullable) NSDictionary<NSString *, IDESImageQualityCheck *> *imageQualityChecks;

/**
 * A unique identifier for this specific processing operation result. It can be used to identify logs for eatch processed document.
 * @return NSString containing UUID for this specific processing request.
 */
@property (nonatomic, readonly, nonnull) NSString *requestId;

/**
 * Returns if the library has been able to recognise this type of document.
 * @return if the library has been able to recognise this type of document.
 */
@property (nonatomic, readonly) BOOL isRecognised;

/**
 * Returns whether the request was totally error-free or library faced non-fatal errors and was be able to proceed with request processing.
 * @return bool indicating not fatal error presens.
 */
@property (nonatomic, readonly) BOOL hadError;

/**
 * Image of processed document.
 * @return UIImage containing processed document representation.
 */
@property (nonatomic, nullable) UIImage *documentImage;

/**
 * Retrieves required action.
 * @return Done or Need_More
 */
@property (nonatomic, readonly, assign) IDESAction action;

@end
