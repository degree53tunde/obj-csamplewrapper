//
//  IDSBase.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#ifndef IDSBASE_H_
#define IDSBASE_H_

#import <Availability.h>
#import <Foundation/Foundation.h>

#ifdef __cplusplus
#define IDS_EXTERN_C_BEGIN      extern "C" {
#define IDS_EXTERN_C_END        }
#else
#define IDS_EXTERN_C_BEGIN
#define IDS_EXTERN_C_END
#endif

#define IDS_STATIC_INLINE       static inline

#ifdef __cplusplus
#define MJCS_EXPORT          extern "C" __attribute__((visibility ("default")))
#else
#define MJCS_EXPORT          extern __attribute__((visibility ("default")))
#endif

#define MJCS_HIDDEN          extern __attribute__((visibility("hidden")))

#define MJCS_CLASS_EXPORT    __attribute__((visibility("default")))

/**
 SDK's error domain
 */
MJCS_EXPORT NSString *const MJCSErrorDomain;

/**
 MJCSErrorCode Defines where you indicate the Error

 - MJCSInvalidArgumentError: occurs when authentication Error, Invalid URL or nil image etc.
 - MJCSInternalError: occurs when internal errors. E.g. When we fail to extract image or one of our modules fail.
 */
typedef NS_ENUM(NSUInteger, MJCSErrorCode) {
    /// It occurs when authentication Error, Invalid URL or nil image etc.
    MJCSInvalidArgumentError = 1,
    
    /// It occurs when internal errors. E.g. When we fail to extract image or one of our modules fail
    MJCSInternalError
};

#endif // #define IDSBASE_H_
