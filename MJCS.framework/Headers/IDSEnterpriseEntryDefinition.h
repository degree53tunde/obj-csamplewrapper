//
//  IDSEnterpriseEntryDefinition.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 15/05/2019.
//  Copyright © 2019 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IDSEnterpriseEntryDefinition : NSObject

/**
 ENUM indicating required step type in enterprise journey

 - actionTypeUndefined: step is unknown
 - actionTypeFrontSide: front side of the document
 - actionTypeBackSide: back side of the document
 - actionTypeSelfie: capture selfie step
 - actionTypeAddressDocument: Proof of Address step
 - actionTypeChipPhotoFaceMatch: RFID chip photo
 - actionTypeLiveness: perform liveness step
 */
typedef NS_ENUM(NSInteger, actionType) {
    actionTypeUndefined = 0,
    
    actionTypeFrontSide = 1,
    
    actionTypeBackSide = 2,
    
    actionTypeSelfie = 3,
    
    actionTypeAddressDocument = 4,
    
    actionTypeChipPhotoFaceMatch = 5,
    
    actionTypeLiveness = 6
};

/**
 ID of the entry definition
 */
@property (nonatomic, nullable, readonly) NSString *journeyEntryDefinitionID;

/**
 ID of the journey definition
 */
@property (nonatomic, nullable, readonly) NSString *journeyDefinitionID;

/**
 Step order in journey definition
 */
@property (nonatomic, assign, readonly) NSInteger order;

/**
 Value indicating required step type in enterprise journey
 */
@property (nonatomic, assign, readonly) actionType type;

/**
 BOOL indicating whether value is optional
 */
@property (nonatomic, assign, readonly) BOOL isOptional;

@end

NS_ASSUME_NONNULL_END
