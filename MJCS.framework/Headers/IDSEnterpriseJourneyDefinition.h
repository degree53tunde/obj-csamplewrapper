//
//  IDSEnterpriseJourneyDefinition.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 14/05/2019.
//  Copyright © 2019 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSEnterpriseEntryDefinition.h"

NS_ASSUME_NONNULL_BEGIN


/**
 Model class, which holds a response from Enterprise Backend listing all possible journeys for mobile channel
 */
@interface IDSEnterpriseJourneyDefinition : NSObject

/**
 ENUM indicating source channel type

 - channelTypeUndefined: Unknown channel type
 - channelTypeCaptureStudio: Capture Studio channel type
 - channelTypeWeb: Web channel type
 - channelTypeMobile: Mobile channel type
 */
typedef NS_ENUM(NSInteger, channelType) {
    channelTypeUndefined = 0,
    
    channelTypeCaptureStudio = 1,
    
    channelTypeWeb = 2,
    
    channelTypeMobile = 3
};

/**
 ENUM indicating the input of media

 - capturingMediaUndefined: Media input is not defined
 - capturingMediaScanner: Scanner media input type, such as 3M physical scanner
 - capturingMediaFileUpload: Input type when uploading a file
 - capturingMediaWebCamera: Input type using web camera
 - capturingMediaCamera: Input type using native mobile camera
 */
typedef NS_ENUM(NSInteger, capturingMedia) {
    capturingMediaUndefined = 0,
    
    capturingMediaScanner = 1,
    
    capturingMediaFileUpload = 2,
    
    capturingMediaWebCamera = 3,
    
    capturingMediaCamera = 4
};

/**
 ID of a digital journey defined in Enterprise backend
 */
@property (nonatomic, nullable, readonly) NSString *journeyDefinitionID;

/**
 Name of the journey
 */
@property (nonatomic, nullable, readonly) NSString *name;

/**
 Value indicating source channel type
 */
@property (nonatomic, assign, readonly) channelType channelType;

/**
 Value indicating the source of media type
 */
@property (nonatomic, assign, readonly) capturingMedia capturingMedia;

/**
 Date when the journey definition was updated
 */
@property (nonatomic, nullable, readonly) NSDate *lastUpdatedDateTime;

/**
 Is journey enabled in enterprise backend
 */
@property (nonatomic, assign, readonly) BOOL isActive;

/**
 Steps that are included inside the journey definition
 */
@property (nonatomic, nullable, readonly) NSArray <IDSEnterpriseEntryDefinition *> *entryDefinitions;

@end

NS_ASSUME_NONNULL_END
