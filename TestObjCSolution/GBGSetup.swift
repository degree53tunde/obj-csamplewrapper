//
//  GBGSetup.swift
//  TestObjCSolution
//
//  Created by Tunde on 01/05/2020.
//  Copyright © 2020 Degree 53 Limited. All rights reserved.
//

import Foundation
import GBGCore
import GBGAddressLookup
import GBGPeopleVerify
import GBGFaceMatch
import GBGSmartCapture
import GBGLiveness

public class TestClass: NSObject {
    
    private var verifyDocumentScanner: GBGVerifyDocumentScanner?

    @objc
    static public func setup() {
        GBGVerifySDK.initialize(GBGCore.self, GBGAddressLookup.self, GBGPeopleVerify.self, GBGFaceMatch.self, GBGSmartCapture.self, GBGLiveness.self)
    }
    
    @objc public func startDocumentScanner() {
       verifyDocumentScanner = GBGVerifyDocumentScanner(scannerType: .document, enableRecaptureDetection: true, delegate: self)
       verifyDocumentScanner?.present()
    }
}

extension TestClass: GBGVerifyDocumentScannerDelegate {
    
    public func documentScannerController(didFinishScanning model: GBGVerifyDocumentScannerModel?) {
        
    }
    
    public func documentScannerControllerDidCancel() {
        
    }
    
    public func documentScannerController(didFailWithError error: Error?) {
        
    }
}
