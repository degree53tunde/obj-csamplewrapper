//
//  ViewController.h
//  TestObjCSolution
//
//  Created by Tunde on 01/05/2020.
//  Copyright © 2020 Degree 53 Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestObjCSolution-Swift.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) TestClass *myTestClass;

@end

