//
//  SceneDelegate.h
//  TestObjCSolution
//
//  Created by Tunde on 01/05/2020.
//  Copyright © 2020 Degree 53 Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

