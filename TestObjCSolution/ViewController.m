//
//  ViewController.m
//  TestObjCSolution
//
//  Created by Tunde on 01/05/2020.
//  Copyright © 2020 Degree 53 Limited. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    _myTestClass = [[TestClass alloc] init];
    [_myTestClass startDocumentScanner];
    
//    *myTestClass = [[TestClass alloc] init];
//    [myTestClass startDocumentScanner];
}


@end
