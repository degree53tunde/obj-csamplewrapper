//
//  IDSLivenessResult.h
//  IDSLiveness
//
//  Created by Edvardas Maslauskas on 24/04/2018.
//  Copyright © 2018 GB Group plc ('GBG'). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSLivenessActionState.h"

/**
 Enum states of perfomed liveness test

 - IDSLivenessResultStateFailed: Liveness detection failed to classify the real human
 - IDSLivenessResultStatePassed: Liveness detection succesfuly finished to clasify a real human
 - IDSLivenessResultStateInterrupted: Liveness detection was interruped while performing a test
 - IDSLivenessResultStateUndefined: Liveness detection test is not defined yet
 */
typedef NS_ENUM(NSInteger, IDSLivenessResultState) {
    IDSLivenessResultStateFailed = 0,
    IDSLivenessResultStatePassed = 1,
    IDSLivenessResultStateInterrupted = 2,
    IDSLivenessResultStateUndefined = 3
};

@interface IDSLivenessResult : NSObject

/**
 Enum, which returns the outcome of liveness detection
 */
@property (nonatomic, readonly) IDSLivenessResultState resultState;

/**
 An array containing all perfomed actions while performing liveness detection test
 */
@property (nonatomic, readonly, nonnull) NSArray<IDSLivenessActionState *> *performedActions;

/**
 @internal 
 */
@property (nonatomic, readonly, nonnull) NSString *c;

@end
